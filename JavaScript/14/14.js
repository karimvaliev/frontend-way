"use strict"

let user = {
    name: 'karim'
}

//local
let id = Symbol('id')

user[id.toString()] = 1;

console.log(user)

//global
let value = Symbol.for('id')

console.log(value)