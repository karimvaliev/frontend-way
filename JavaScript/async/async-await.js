const sendRequestBtn = document.getElementById('sendRequest')
const inputUrl = document.getElementById('input')

let list = document.getElementById('jsonResponse')

function sendRequest(url){
    list.innerHTML = 'Loading...'
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            const rs = await fetch(url)
            if (rs.ok) {
                resolve(rs.json())
            } else {
                reject('ERROR')
            }
            list.innerHTML = ''
        }, 3000)
    })
}

function putElemToList(key, value) {
    let elem = document.createElement('li')
    elem.appendChild(document.createTextNode(`"${key}": "${value}"`))
    elem.setAttribute('id', key)
    list.appendChild(elem)
}

function renderObject(obj) {
    Object.entries(obj).forEach((entry => {
        const [key, value] = entry;
        putElemToList(key, value);
    }))
}

sendRequestBtn.onclick = async function () {
    list.innerHTML = ''
    const url = inputUrl.value
    console.log('1')
    sendRequest(url)
        .then(json => console.log('2'))
        .catch(error => console.log(error));
    console.log('3')
}