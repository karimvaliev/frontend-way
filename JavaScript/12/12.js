"use strict"

let user = {
    name: 'Karim',
    age: 20,
    arr: [{
            id: 2,
            epk: 3
        },
        {
            id: 3,
            epk: 3
        }]
}

let copyUser = user;

copyUser['name'] = 'change';

console.log(copyUser)

console.log(copyUser === user)