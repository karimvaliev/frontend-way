"use strict"

readNumber()

function readNumber() {
    let num;
    while (true) {
        num = prompt('Number')
        if(num === '' || num === null || !isFinite(+num)) {
            continue ;
        }
        break ;
    }
    alert(num)
}