"use strict"

let range = {
    from: 1,
    to: 5,
}

range[Symbol.iterator] = function () {
    return {
        current: this.from,
        last: this.to,

        next() {
            // 4. он должен вернуть значение в виде объекта {done:.., value :...}
            if (this.current <= this.last) {
                return { done: false, value: this.current++ };
            } else {
                return { done: true };
            }
        }
    };
}

console.log(range)

// for (let num of range) {
//     alert(num); // 1, затем 2, 3, 4, 5
// }