"use strict"

function User(name, age) {
    if(!new.target) {
        return new User(name, age)
    }
    this.name = name;
    this.age = age;
    this.foo = function (name, age) {
        console.log('My name is ', this.name, ' ', this.age)
    }
}

const user = User('Karim', 20);

console.log(user.foo());