"use strict"

let ret = 'Hello world'

let foo = () => {
    console.log('Arrow function')
    return 'End'
}

console.log(foo())