"use strict"

let styles = ['Джаз', 'Блюз']

styles.push('Рок-н-ролл')

function changeMiddleValue(newValue) {
    let middle = Math.round(styles.length / 2);
    styles[middle] = newValue;
}

changeMiddleValue('New Value')

console.log(styles)

let pop = styles.shift();

console.log(pop)
console.log(styles)

styles.unshift('Рэп', 'Регги')

console.log(styles)