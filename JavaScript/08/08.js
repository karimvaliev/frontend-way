"use strict"

initStatement()
initStatementWithoutParam()

function initStatement() {
    let name = 'Karim'
    newValue()
    console.log(name)
}

function initStatementWithoutParam() {
    let name = 2
    newValue()
    console.log(name)
}

function newValue(str = function () {
    return 'Значение не передано'
}) {
    console.log(str())
}