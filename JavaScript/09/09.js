"use strict"

foo()

function foo() {
    let foo = function () {
        console.log('Hello world!')
    }
    console.log(foo)
}