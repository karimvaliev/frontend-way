"use strict"

let user = {
    name: 'Karim',
    age: 20,
    arr: [{
            id: 2,
            epk: 3
        },
        {
            id: 3,
            epk: 3
        }]
}

for(let key in user) {
    console.log(user[key])
}

console.log("arr" in user)

user.isAdmin = true;

console.log(user)

delete user.isAdmin

console.log(user)

user['Is Admin'] = true;

console.log(user['Is Admin'])

delete user['Is Admin']

let name = prompt('Name')

user[name] = 'Karim'

console.log(user)