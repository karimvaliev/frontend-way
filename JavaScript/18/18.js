"use strict"

function ucFirst(str) {
    return str[0].toUpperCase() + str.slice(1);
}

console.log(ucFirst('hello') === 'Hello')

function checkSpam(str) {
    let lowStr = str.toLowerCase();
    return lowStr.includes('viagra') || lowStr.includes('xxx');
}

console.log(checkSpam('buy ViAgRA now'))
console.log(checkSpam('free xxxxx'))
console.log(checkSpam("innocent rabbit"))

function truncate(str, maxlength) {
    if(str.length > maxlength) {
        return str.slice(0, maxlength - 1) + '...'
    }
    return str
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 20))