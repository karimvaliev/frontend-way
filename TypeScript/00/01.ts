// function foo(name: string, age: number) {
//     console.log(`My name is ${name}, age is ${age}`)
// }
//
// foo('Karim', 22);

// function foo2(name: string): string {
//     console.log(name);
//     return name
// }
//
// function foo3(name: string): string {
//     console.log(name + ' ' + name);
//     return name
// }
//
// let foo: (name: string) => string = foo2;
//
// foo('Karim');
//
// foo = foo3;
//
// foo('Karim');

type User = {
    name: string,
    age?: number
};

type Employee = User & {
    role?: string
}

function foo(name: User): User {
    if(name.age === undefined) {
        return null;
    }
    return name;
}

let karim: User = {
    name: 'Karim',
    age: 20
}


let nikita: Employee = {
    name: 'Karim',
    age: 30,
    role: 'Manager'
}

console.log(foo(karim))
console.log(foo(nikita))

