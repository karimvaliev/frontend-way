// function foo(name: string, age: number) {
//     console.log(`My name is ${name}, age is ${age}`)
// }
//
// foo('Karim', 22);
function foo(name) {
    if (name.age === undefined) {
        return null;
    }
    return name;
}
var karim = {
    name: 'Karim',
    age: 20
};
var nikita = {
    name: 'Karim',
    age: 30,
    role: 'Manager'
};
console.log(foo(karim));
console.log(foo(nikita));
