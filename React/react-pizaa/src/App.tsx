import './scss/app.scss';
import Header from './components/header';
import Categories from "./components/categories";
import Sort from "./components/sort";
import Pizza from "./components/pizza";

function App() {
    return (
        <div className="wrapper">
            <Header/>
            <div className="content">
                <div className="container">
                    <div className="content__top">
                        <Categories/>
                        <Sort/>
                    </div>
                    <h2 className="content__title">Все пиццы</h2>
                    <Pizza/>
              </div>
          </div>
      </div>
  );
}

export default App;
