import React, {useState} from "react";
import CategoriesJson from "../assets/json/categories.json";
import {CategoryType} from "../types";

export default function Categories() {
    const [active, setActive] = useState(0);

    // Типо делаем запрос на бек
    const categories: CategoryType[] = CategoriesJson;

    return (
        <div className="categories">
            <ul>
                {categories.map(category => {
                    return <li onClick={() => setActive(category.id)}
                               className={active === category.id ? "active" : ""} key={category.id}>{category.text}</li>
                })}
            </ul>
        </div>
    )
}