import React, {useEffect, useState} from "react";
import JsonPizza from "../assets/json/pizzas.json"
import {PizzaType} from "../types";

const pizzaTypes = ['Тонкое', 'Традиционное']

type Rating = {
    rating: number
}

function RenderRating({rating}: any) {
    return (
        <div className="pizza-block__rating">
            {
                [...Array(rating)].map((value, index, array) => {
                    return <img width="30"
                                src="https://png.pngtree.com/png-vector/20231019/ourmid/pngtree-one-stars-rating-png-png-image_10262742.png"
                                alt="Рейтинг"
                                key={index}/>
                })
            }
        </div>
    )
}

function PizzaTemplate(pizza: PizzaType) {
    const [count, setCount] = useState(0);
    const [activeSize, setActiveSize] = useState(0)
    const [activeType, setActiveType] = useState(0)

    return (
        <div className="pizza-block" key={pizza.id}>
            <img
                className="pizza-block__image"
                src={pizza.imageUrl}
                alt="Pizza"
            />
            <h4 className="pizza-block__title">{pizza.title}</h4>
            <div className="pizza-block__selector">
                <ul>
                    {pizza.types.map(type => <li onClick={() => setActiveType(type)}
                                                 className={activeType === type ? "active" : ""} key={type}>{pizzaTypes[type]}</li>)}
                </ul>
                <ul>
                    {pizza.sizes.map(size => (
                        <li onClick={() => setActiveSize(size)}
                            className={activeSize === size ? "active" : ""} key={size}>{size} cm</li>
                    ))}
                </ul>
                <RenderRating rating={pizza.rating}/>
            </div>
            <div className="pizza-block__bottom">
                <div className="pizza-block__price">от {pizza.price}</div>
                <button onClick={() => {
                    setCount(count + 1)
                }} className="button button--outline button--add">
                    <svg
                        width="12"
                        height="12"
                        viewBox="0 0 12 12"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                            fill="white"
                        />
                    </svg>
                    <span>Добавить</span>
                    <i>{count}</i>
                </button>
            </div>
        </div>
    )
}

export default function Pizza() {
    const [pizzas, setPizza] = useState<PizzaType[] | null>(null)

    const getData = async () => {
        await fetch("https://65c3944f39055e7482c12fff.mockapi.io/pizza/pizzas")
            .then((response): Promise<PizzaType[]> => response.json())
            .then(pizzasJson => setPizza(pizzasJson));
    }

    // Типо получаю ответ от бека
    useEffect(() => {
         getData()
    }, []);

    return (
        <div className="content__items">
            {pizzas && pizzas!.map(pizza => PizzaTemplate(pizza))}
        </div>
    )
}