import React, {useState} from "react";
const sortValues = ['Популярности', 'Цене', 'Алфавиту']

export default function Sort() {
    const [isVisible, setIsVisible] = useState(false);
    const [sortValue, setSortValue] = useState(0)

    return (
        <div className="sort">
            <div className="sort__label">
                <b>Сортировка по:</b>
                <span onClick={() => setIsVisible(!isVisible)}>{sortValues[sortValue]}</span>
            </div>
                {isVisible && (<div className="sort__popup">
                    <ul>
                        {sortValues.map((value, index) => <li onClick={() => {
                            setIsVisible(!isVisible)
                            setSortValue(index)
                        }} className={sortValue === index ? "active" : ""} key={index}>{sortValues[index]}</li>)}
                    </ul>
                </div>)}
        </div>
    )
}
